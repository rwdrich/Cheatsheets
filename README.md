## Synopsis
<!---
        At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)
--->

This repository contains any cheat sheets that I have come across in my time as a coder. I hold no responsibility for the accuracy of the documents.

## Motivation
<!---
        A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.
--->

The motivation behind this is such that I have a centralised version of these documents, and so that I can access them remotely. I have also made this repository public, such that anyone can use the documents herein.


## Installation
<!---
        Provide code examples and explanations of how to get the project.
--->

It's just a file structure, just clone it and use the documents as you wish.

## Contributors
<!---
        Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.
--->

I don't know the contributors to these files, I have just downloaded them when I've found them. If you would like to be credited for the creation of one of these files, or would like to contribute a cheat sheet please message me.


## License
<!---
        A short snippet describing the license (MIT, Apache, etc.)
--->

I provide no licensing for this work, most of the work is not my own, and the work that is is welcome to be used, copied, distributed, whatever.
